package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity2 extends AppCompatActivity {
    String cityname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        cityname = intent.getStringExtra("cityname");
        weather();

    }
    public void weather()
    {
        String URL = "http://api.weatherstack.com/current?access_key=56fefdf8844b69c9d84b4cf467f3e78c&query="+cityname;
        JSONObject jsonObject=new JSONObject();

        Response.Listener<JSONObject> successBlock=new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject location = response.getJSONObject("location");
                    JSONObject current = response.getJSONObject("current");
                    JSONArray description = current.getJSONArray("weather_descriptions");
                    String name = location.getString("name");
                    String country = location.getString("country");
                    String temp = current.getString("temperature");
                    String desc = description.getString(0);
                    String windspeed = current.getString("wind_speed");
                    String winddir = current.getString("wind_dir");
                    String precip = current.getString("precip");
                    String humidity = current.getString("humidity");
                    TextView ecountry = findViewById(R.id.textView5);
                    TextView etemp = findViewById(R.id.textView6);
                    TextView edesc = findViewById(R.id.textView7);
                    TextView ewindspeed = findViewById(R.id.textView8);
                    TextView ewinddir = findViewById(R.id.textView9);
                    TextView eprecip = findViewById(R.id.textView10);
                    TextView ehumidity = findViewById(R.id.textView11);
                    ecountry.setText(name+","+country);
                    etemp.setText("Temperature : "+temp+"°C");
                    edesc.setText("Description : "+desc);
                    ewindspeed.setText("Wind Speed : "+windspeed);
                    ewinddir.setText("Wind Direction : "+winddir);
                    eprecip.setText("Precipitation : "+precip);
                    ehumidity.setText("Humidity : "+humidity);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };

        Response.ErrorListener failureBlock = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity2.this, "Error", Toast.LENGTH_SHORT).show();
            }
        };


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, successBlock, failureBlock);
        Volley.newRequestQueue(this).add(request);
    }
}